#include <iostream>
#include <vector>
#include <memory>
#include <chrono>
#include <ctime>
#include <thread>
#include <queue>
#include <mutex>

struct SafeThread {
    ::std::thread _t;

    SafeThread(::std::thread && t) : _t(::std::move(t)) {}

    SafeThread(SafeThread && other) : _t(::std::move(other._t)) {}

    SafeThread(SafeThread const & other) = delete;

    ~SafeThread() {
        if (_t.joinable()) {
            _t.join();
        }
    }

    void join() {
        if (_t.joinable()) {
            _t.join();
        }
    }
};

enum Pizza {
    Margherita = 0,
    Vegetarian,
    Mexican
};

struct Order
{
    Order() = delete;

    Order(uint32_t id, Pizza pizza) : _id(id), _pizza(pizza) {
        
    }

    ~Order() {
        
    }

    Pizza _pizza;
    uint32_t _id;
};

template<typename T>
struct ThreadSafeQueue {
    ::std::queue<::std::unique_ptr<T>> _data;
    ::std::mutex _lock;

    ThreadSafeQueue() {}

    void addElement(::std::unique_ptr<T> && element) {
        ::std::lock_guard<::std::mutex> locker(this->_lock);
        this->_data.push(::std::move(element));
    }

    ::std::unique_ptr<T> getNextThreadSafeQueue() {
        ::std::lock_guard<::std::mutex> locker(this->_lock);
        if (this->_data.size() == 0)
        {
            return ::std::unique_ptr<T>(nullptr);
        }
        ::std::unique_ptr<T> o = ::std::move(this->_data.front());
        this->_data.pop();
        return ::std::move(o);
    }

    size_t getSize() {
        ::std::lock_guard<::std::mutex> locker(this->_lock);
        return this->_data.size();
    }
};

struct Cook
{
    Cook() = delete;

    Cook(::std::string const & name) : _name(name) {
    }

    ~Cook() {
    }

    void run(ThreadSafeQueue<Order> & orders) const {
        while (true) {
            ::std::unique_ptr<Order> o = orders.getNextThreadSafeQueue();
            if (o != nullptr) {
                ::std::cout << this->_name << " is taking order #" << o->_id << "." << ::std::endl;
                ::std::this_thread::sleep_for(::std::chrono::milliseconds((1500 + ::std::rand()) % 3000));
                ::std::cout << "order #" << o->_id << " done." << ::std::endl;
            }
        }
    }

    ::std::string _name;
};

struct Pizzeria
{
    ::std::vector<::std::unique_ptr<Cook>> cooks;
    ThreadSafeQueue<Order> orders;

    void recruit(::std::unique_ptr<Cook> && cook) {
        ::std::cout << "Recruiting " << cook->_name;
        this->cooks.push_back(::std::move(cook));
        ::std::cout << ", " << this->cooks.size() << " cook(s) in pizzeria" << ::std::endl;
    }

    void open() {
        SafeThread customerGenerator(::std::thread([&]() {
            while (true) {
                Pizza randomPizza = static_cast<Pizza>(::std::rand() % 3);
                orders.addElement(::std::make_unique<Order>(::std::rand() % 1000, randomPizza));
                ::std::this_thread::sleep_for(::std::chrono::milliseconds(250));
            }
        }));

        ::std::vector<SafeThread> cookWorkers;
        for (auto const & cook : cooks) {
            cookWorkers.push_back(SafeThread(::std::thread([&]() {
                cook->run(orders);
            })));
        }

        SafeThread statusWriterWorker(::std::thread([&]() {
            while (true) {
                ::std::cout << "                 " << orders.getSize() << " orders remaining..." << ::std::endl;
                ::std::this_thread::sleep_for(::std::chrono::milliseconds(1000));
            }
        }));

        statusWriterWorker.join();
        customerGenerator.join();
        for (auto & cookWorker : cookWorkers) {
            cookWorker.join();
        }
    }
};


int main()
{
    ::std::cout << "Welcome to my pizzeria..." << ::std::endl;
    Pizzeria pizzeria;

    pizzeria.recruit(::std::make_unique<Cook>("G. Ramsay"));
    pizzeria.recruit(::std::make_unique<Cook>("C. Lignac"));
    pizzeria.recruit(::std::make_unique<Cook>("P. Etchebest"));
    pizzeria.recruit(::std::make_unique<Cook>("G. Goujon"));
    pizzeria.recruit(::std::make_unique<Cook>("L. Petit"));

    pizzeria.open();
    return 0;
}